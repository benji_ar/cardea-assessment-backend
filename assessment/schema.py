import graphene

import assessment.lists.schema
import assessment.lists.mutations
import assessment.jobs.schema
import assessment.jobs.mutations

class Query(
    assessment.lists.schema.ListQuery, assessment.jobs.schema.JobQuery, graphene.ObjectType
):
    # This class extends all abstract apps level Queries and graphene.ObjectType
    pass

class Mutation(assessment.lists.mutations.Mutation, assessment.jobs.mutations.Mutation, graphene.ObjectType):
    pass


schema = graphene.Schema(query=Query, mutation=Mutation)


