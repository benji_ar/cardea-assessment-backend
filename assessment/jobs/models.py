from django.contrib import admin
from django.db import models

from assessment.lists.models import List

class Job(models.Model):
    job_title = models.CharField(max_length=200)
    company = models.CharField(max_length=200)
    location = models.CharField(max_length=200)
    min_salary = models.IntegerField()
    max_salary = models.IntegerField()
    apply_link = models.URLField()
    lists = models.ManyToManyField(List)

    def __str__(self):
      return self.job_title

    def job_lists(self):
      return ', '.join([l.name for l in self.lists.all()])
    job_lists.short_description = "Job Lists"