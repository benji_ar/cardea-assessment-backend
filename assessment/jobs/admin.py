from django.contrib import admin
from assessment.jobs.models import Job

class JobsAdmin(admin.ModelAdmin):
    list_display = ('id', 'job_title', 'company', 'job_lists')

admin.site.register(Job, JobsAdmin)

