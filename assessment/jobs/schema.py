import graphene
from graphene_django import DjangoObjectType
from assessment.jobs.models import Job as JobModel
# this creates a GraphQL type from the Django model
class JobType(DjangoObjectType):
   class Meta:
       model = JobModel
       fields = "__all__"

# this creates the GraphQL queries for the List GraphQL object
class JobQuery(graphene.AbstractType):
    job_by_id = graphene.Field(JobType, id=graphene.Int())
    all_jobs = graphene.List(JobType)
    jobs_by_list = graphene.List(JobType, ids=graphene.List(graphene.Int))

    def resolve_all_jobs(self, info):
        return JobModel.objects.all()

    def resolve_job_by_id(self, info, id):
        return JobModel.objects.filter(id=id)

    def resolve_jobs_by_list(self, info, ids):
        return JobModel.objects.filter(lists__id__in=ids).distinct()
