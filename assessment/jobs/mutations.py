import graphene
from .schema import JobType
from .models import Job as JobModel
from assessment.lists.models import List


class JobInput(graphene.InputObjectType):
    job_title = graphene.String()
    company = graphene.String()
    location = graphene.String()
    min_salary = graphene.Int()
    max_salary = graphene.Int()
    apply_link = graphene.String()
    lists = graphene.List(graphene.Int)


class CreateJob(graphene.Mutation):
    # define the arguments that the mutation takes
    class Arguments:
        input = JobInput(required=True)

    # define the values that get returned after the mutation is resolved
    ok = graphene.Boolean()
    job = graphene.Field(JobType)

    @staticmethod 
    def mutate(root, info, input=None):
        ok = True
        lists = []
        for job_list_id in input.lists:
            l = List.objects.get(pk=job_list_id)
            if l is None:
                return CreateJob(ok=False, job=None)
            lists.append(l)
        job_instance = JobModel(job_title=input.job_title, company=input.company, location=input.location, min_salary=input.min_salary, max_salary=input.max_salary, apply_link=input.apply_link)
        job_instance.save()
        job_instance.lists.set(lists)
        return CreateJob(ok=True, job=job_instance)

class Mutation(graphene.ObjectType):
    create_job = CreateJob.Field()