import graphene
from .schema import ListType
from .models import List as ListModel

class ListInput(graphene.InputObjectType):
    name= graphene.String(required=True)
    description = graphene.String(required=True)
    image = graphene.String(required=True)
    list_curator = graphene.String(required=True)


class CreateList(graphene.Mutation):
    # define the arguments that the mutation takes
    class Arguments:
        input = ListInput(required=True)

    # define the values that get returned after the mutation is resolved
    ok = graphene.Boolean()
    job_list = graphene.Field(ListType)

    @staticmethod 
    def mutate(root, info, input=None):
        list_instance = ListModel(name=input.name, description=input.description, image=input.image, list_curator=input.list_curator)
        list_instance.save()
        return CreateList(ok=True, job_list=list_instance)

class Mutation(graphene.ObjectType):
    create_list = CreateList.Field()