from django.contrib import admin
from assessment.lists.models import List

class ListAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'list_curator')

admin.site.register(List, ListAdmin)

