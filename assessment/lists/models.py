from django.db import models

'''
type List {
  id: ID!
  name: String!
  description: String!
  image: url!
  list_curator: string!     
}
'''

class List(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    image = models.URLField()
    list_curator = models.CharField(max_length=200)


    def __str__(self):
        return self.name