import graphene
from graphene_django import DjangoObjectType
from assessment.lists.models import List as ListModel
# this creates a GraphQL type from the Django model
class ListType(DjangoObjectType):
   class Meta:
       model = ListModel
       fields = "__all__"

# this creates the GraphQL queries for the List GraphQL object
class ListQuery(graphene.AbstractType):
    list_by_id = graphene.Field(ListType, id=graphene.Int())
    all_lists = graphene.List(ListType)
    lists = graphene.List(ListType, ids=graphene.List(graphene.Int))

    def resolve_all_lists(self, info):
        return ListModel.objects.all()

    def resolve_list_by_id(self, info, id):
        return ListModel.objects.get(id=id)

    def resolve_lists(self, info, ids):
        return ListModel.objects.filter(id__in=ids)
